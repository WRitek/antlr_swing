package tb.antlr.symbolTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class GlobalSymbols {
	
	LinkedHashMap<String, Integer> memory = new LinkedHashMap<>();

	public GlobalSymbols() {
	}
	
	public boolean hasSymbol(String name) {
		return memory.containsKey(name);
	}

	public void newSymbol(String name) throws RuntimeException{
		if (! hasSymbol(name))
			memory.put(name, null);
		else
			throw new RuntimeException("Variable " + name +" exists!");
	}
	
	public void setSymbol(String name, Integer value) throws RuntimeException {
		if( hasSymbol(name))
			memory.put(name, value);
		else
			throw new RuntimeException("Variable " + name +" does not exist!");
	}
	
	public Integer getSymbol(String name) throws RuntimeException {
		if( hasSymbol(name))
			return memory.get(name);
		else
			throw new RuntimeException("Variable " + name +" does not exist!");
	}
	
	
	public Integer addVars() {
		List<String> listKey = new ArrayList<String>(memory.keySet());
		List listVal = new ArrayList<Integer>(memory.values());
		
		Integer i1 = (Integer) listVal.get(listVal.size()-1);
		Integer i2 = (Integer) listVal.get(listVal.size()-2);
		String key1 = listKey.get(listKey.size()-1);
		String key2 = listKey.get(listKey.size()-2);
		
		try {
			
			memory.put(key1, i1+i2);
			memory.remove(key2);
			
			return i1+i2;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public Integer subVars() {
		List<String> listKey = new ArrayList<String>(memory.keySet());
		List listVal = new ArrayList<Integer>(memory.values());
		
		Integer i1 = (Integer) listVal.get(listVal.size()-1);
		Integer i2 = (Integer) listVal.get(listVal.size()-2);
		String key1 = listKey.get(listKey.size()-1);
		String key2 = listKey.get(listKey.size()-2);
		
		try {
			
			memory.put(key1, i1-i2);
			memory.remove(key2);
			
			return i1-i2;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public Integer mulVars() {
		List<String> listKey = new ArrayList<String>(memory.keySet());
		List listVal = new ArrayList<Integer>(memory.values());
		
		Integer i1 = (Integer) listVal.get(listVal.size()-1);
		Integer i2 = (Integer) listVal.get(listVal.size()-2);
		String key1 = listKey.get(listKey.size()-1);
		String key2 = listKey.get(listKey.size()-2);
		
		try {
			
			memory.put(key1, i1*i2);
			memory.remove(key2);
			
			return i1*i2;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public Integer divVars() {
		List<String> listKey = new ArrayList<String>(memory.keySet());
		List listVal = new ArrayList<Integer>(memory.values());
		
		Integer i1 = (Integer) listVal.get(listVal.size()-1);
		Integer i2 = (Integer) listVal.get(listVal.size()-2);
		String key1 = listKey.get(listKey.size()-1);
		String key2 = listKey.get(listKey.size()-2);
		
		try {
			
			memory.put(key1, i1/i2);
			memory.remove(key2);
			
			return i1/i2;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public Integer movA(String name) {
		List<String> listKey = new ArrayList<String>(memory.keySet());
		List listVal = new ArrayList<Integer>(memory.values());
		
		String key1 = listKey.get(listKey.size()-1);
		
		Integer val = getSymbol(name);
		memory.put(key1, val);
		
		return val;
	}
	
	public Integer movN(String name) {
		List<String> listKey = new ArrayList<String>(memory.keySet());
		List listVal = new ArrayList<Integer>(memory.values());
		
		String key1 = listKey.get(listKey.size()-1);
		Integer val = getSymbol(key1);
		//Integer val = (Integer) listVal.get(listVal.size()-1);
		
		memory.put(name, val);
		
		return val;
	}
	
	
}
