grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!;
    
blok : BEGIN^ (stat | blok)* END!
;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    | MOVA ID NL -> ^(MOVA ID)
    | MOVN ID NL -> ^(MOVN ID)
    | HASH ID NL -> ^(HASH ID)
    
    | NL ->
    ;
    
if_stat 
  : IF^ expr THEN! (expr) (ELSE! (expr))?
  ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ADD
    | SUB
    | MULT
    | DIVI
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF : 'if';
ELSE : 'else';
THEN : 'then';

BEGIN : '{';
END : '}';
//====
ADD : 'ADD';
SUB : 'SUB';
MULT : 'MULT';
DIVI : 'DIVI';
MOVA : 'MOVA';
MOVN : 'MOVN';
//====
ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
HASH 
  : '#'
  ;

