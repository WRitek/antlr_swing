// $ANTLR 3.5.1 /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2020-03-22 22:55:32

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ExprLexer extends Lexer {
	public static final int EOF=-1;
	public static final int BEGIN=4;
	public static final int DIV=5;
	public static final int ELSE=6;
	public static final int END=7;
	public static final int ID=8;
	public static final int IF=9;
	public static final int INT=10;
	public static final int LP=11;
	public static final int MINUS=12;
	public static final int MUL=13;
	public static final int NL=14;
	public static final int PLUS=15;
	public static final int PODST=16;
	public static final int RP=17;
	public static final int THEN=18;
	public static final int VAR=19;
	public static final int WS=20;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ExprLexer() {} 
	public ExprLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ExprLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:57:5: ( 'var' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:57:6: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:4: ( 'if' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:6: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:60:6: ( 'else' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:60:8: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:6: ( 'then' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:8: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "BEGIN"
	public final void mBEGIN() throws RecognitionException {
		try {
			int _type = BEGIN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:63:7: ( '{' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:63:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BEGIN"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:64:5: ( '}' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:64:7: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:66:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:66:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:66:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:68:5: ( ( '0' .. '9' )+ )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:68:7: ( '0' .. '9' )+
			{
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:68:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			int _type = NL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:70:4: ( ( '\\r' )? '\\n' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:70:6: ( '\\r' )? '\\n'
			{
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:70:6: ( '\\r' )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='\r') ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:70:6: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:72:4: ( ( ' ' | '\\t' )+ )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:72:6: ( ' ' | '\\t' )+
			{
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:72:6: ( ' ' | '\\t' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0=='\t'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "LP"
	public final void mLP() throws RecognitionException {
		try {
			int _type = LP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:76:2: ( '(' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:76:4: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LP"

	// $ANTLR start "RP"
	public final void mRP() throws RecognitionException {
		try {
			int _type = RP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:80:2: ( ')' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:80:4: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RP"

	// $ANTLR start "PODST"
	public final void mPODST() throws RecognitionException {
		try {
			int _type = PODST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:84:2: ( '=' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:84:4: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PODST"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:88:2: ( '+' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:88:4: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:92:2: ( '-' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:92:4: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:96:2: ( '*' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:96:4: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:100:2: ( '/' )
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:100:4: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:8: ( VAR | IF | ELSE | THEN | BEGIN | END | ID | INT | NL | WS | LP | RP | PODST | PLUS | MINUS | MUL | DIV )
		int alt5=17;
		switch ( input.LA(1) ) {
		case 'v':
			{
			int LA5_1 = input.LA(2);
			if ( (LA5_1=='a') ) {
				int LA5_18 = input.LA(3);
				if ( (LA5_18=='r') ) {
					int LA5_22 = input.LA(4);
					if ( ((LA5_22 >= '0' && LA5_22 <= '9')||(LA5_22 >= 'A' && LA5_22 <= 'Z')||LA5_22=='_'||(LA5_22 >= 'a' && LA5_22 <= 'z')) ) {
						alt5=7;
					}

					else {
						alt5=1;
					}

				}

				else {
					alt5=7;
				}

			}

			else {
				alt5=7;
			}

			}
			break;
		case 'i':
			{
			int LA5_2 = input.LA(2);
			if ( (LA5_2=='f') ) {
				int LA5_19 = input.LA(3);
				if ( ((LA5_19 >= '0' && LA5_19 <= '9')||(LA5_19 >= 'A' && LA5_19 <= 'Z')||LA5_19=='_'||(LA5_19 >= 'a' && LA5_19 <= 'z')) ) {
					alt5=7;
				}

				else {
					alt5=2;
				}

			}

			else {
				alt5=7;
			}

			}
			break;
		case 'e':
			{
			int LA5_3 = input.LA(2);
			if ( (LA5_3=='l') ) {
				int LA5_20 = input.LA(3);
				if ( (LA5_20=='s') ) {
					int LA5_24 = input.LA(4);
					if ( (LA5_24=='e') ) {
						int LA5_27 = input.LA(5);
						if ( ((LA5_27 >= '0' && LA5_27 <= '9')||(LA5_27 >= 'A' && LA5_27 <= 'Z')||LA5_27=='_'||(LA5_27 >= 'a' && LA5_27 <= 'z')) ) {
							alt5=7;
						}

						else {
							alt5=3;
						}

					}

					else {
						alt5=7;
					}

				}

				else {
					alt5=7;
				}

			}

			else {
				alt5=7;
			}

			}
			break;
		case 't':
			{
			int LA5_4 = input.LA(2);
			if ( (LA5_4=='h') ) {
				int LA5_21 = input.LA(3);
				if ( (LA5_21=='e') ) {
					int LA5_25 = input.LA(4);
					if ( (LA5_25=='n') ) {
						int LA5_28 = input.LA(5);
						if ( ((LA5_28 >= '0' && LA5_28 <= '9')||(LA5_28 >= 'A' && LA5_28 <= 'Z')||LA5_28=='_'||(LA5_28 >= 'a' && LA5_28 <= 'z')) ) {
							alt5=7;
						}

						else {
							alt5=4;
						}

					}

					else {
						alt5=7;
					}

				}

				else {
					alt5=7;
				}

			}

			else {
				alt5=7;
			}

			}
			break;
		case '{':
			{
			alt5=5;
			}
			break;
		case '}':
			{
			alt5=6;
			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case '_':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'f':
		case 'g':
		case 'h':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 'u':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt5=7;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt5=8;
			}
			break;
		case '\n':
		case '\r':
			{
			alt5=9;
			}
			break;
		case '\t':
		case ' ':
			{
			alt5=10;
			}
			break;
		case '(':
			{
			alt5=11;
			}
			break;
		case ')':
			{
			alt5=12;
			}
			break;
		case '=':
			{
			alt5=13;
			}
			break;
		case '+':
			{
			alt5=14;
			}
			break;
		case '-':
			{
			alt5=15;
			}
			break;
		case '*':
			{
			alt5=16;
			}
			break;
		case '/':
			{
			alt5=17;
			}
			break;
		default:
			NoViableAltException nvae =
				new NoViableAltException("", 5, 0, input);
			throw nvae;
		}
		switch (alt5) {
			case 1 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:10: VAR
				{
				mVAR(); 

				}
				break;
			case 2 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:14: IF
				{
				mIF(); 

				}
				break;
			case 3 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:17: ELSE
				{
				mELSE(); 

				}
				break;
			case 4 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:22: THEN
				{
				mTHEN(); 

				}
				break;
			case 5 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:27: BEGIN
				{
				mBEGIN(); 

				}
				break;
			case 6 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:33: END
				{
				mEND(); 

				}
				break;
			case 7 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:37: ID
				{
				mID(); 

				}
				break;
			case 8 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:40: INT
				{
				mINT(); 

				}
				break;
			case 9 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:44: NL
				{
				mNL(); 

				}
				break;
			case 10 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:47: WS
				{
				mWS(); 

				}
				break;
			case 11 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:50: LP
				{
				mLP(); 

				}
				break;
			case 12 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:53: RP
				{
				mRP(); 

				}
				break;
			case 13 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:56: PODST
				{
				mPODST(); 

				}
				break;
			case 14 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:62: PLUS
				{
				mPLUS(); 

				}
				break;
			case 15 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:67: MINUS
				{
				mMINUS(); 

				}
				break;
			case 16 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:73: MUL
				{
				mMUL(); 

				}
				break;
			case 17 :
				// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:77: DIV
				{
				mDIV(); 

				}
				break;

		}
	}



}
