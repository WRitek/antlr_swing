// $ANTLR 3.5.1 /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2020-03-22 22:55:32

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ExprParser extends DebugParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "BEGIN", "DIV", "ELSE", "END", 
		"ID", "IF", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "RP", 
		"THEN", "VAR", "WS"
	};
	public static final int EOF=-1;
	public static final int BEGIN=4;
	public static final int DIV=5;
	public static final int ELSE=6;
	public static final int END=7;
	public static final int ID=8;
	public static final int IF=9;
	public static final int INT=10;
	public static final int LP=11;
	public static final int MINUS=12;
	public static final int MUL=13;
	public static final int NL=14;
	public static final int PLUS=15;
	public static final int PODST=16;
	public static final int RP=17;
	public static final int THEN=18;
	public static final int VAR=19;
	public static final int WS=20;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public static final String[] ruleNames = new String[] {
		"invalidRule", "multExpr", "if_stat", "prog", "expr", "atom", "blok", 
		"stat"
	};

	public static final boolean[] decisionCanBacktrack = new boolean[] {
		false, // invalid decision
		false, false, false, false, false, false, false, false, false, false
	};

 
	public int ruleLevel = 0;
	public int getRuleLevel() { return ruleLevel; }
	public void incRuleLevel() { ruleLevel++; }
	public void decRuleLevel() { ruleLevel--; }
	public ExprParser(TokenStream input) {
		this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
	}
	public ExprParser(TokenStream input, int port, RecognizerSharedState state) {
		super(input, state);
		DebugEventSocketProxy proxy =
			new DebugEventSocketProxy(this,port,adaptor);
		setDebugListener(proxy);
		setTokenStream(new DebugTokenStream(input,proxy));
		try {
			proxy.handshake();
		}
		catch (IOException ioe) {
			reportError(ioe);
		}
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);
		proxy.setTreeAdaptor(adap);
	}

	public ExprParser(TokenStream input, DebugEventListener dbg) {
		super(input, dbg);
		 
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);

	}

	protected boolean evalPredicate(boolean result, String predicate) {
		dbg.semanticPredicate(result, predicate);
		return result;
	}

		protected DebugTreeAdaptor adaptor;
		public void setTreeAdaptor(TreeAdaptor adaptor) {
			this.adaptor = new DebugTreeAdaptor(dbg,adaptor);
		}
		public TreeAdaptor getTreeAdaptor() {
			return adaptor;
		}
	@Override public String[] getTokenNames() { return ExprParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }


	public static class prog_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "prog"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat | blok )+ EOF !;
	public final ExprParser.prog_return prog() throws RecognitionException {
		ExprParser.prog_return retval = new ExprParser.prog_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EOF3=null;
		ParserRuleReturnScope stat1 =null;
		ParserRuleReturnScope blok2 =null;

		CommonTree EOF3_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "prog");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(16, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat | blok )+ EOF !)
			dbg.enterAlt(1);

			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat | blok )+ EOF !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(17,7);
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat | blok )+
			int cnt1=0;
			try { dbg.enterSubRule(1);

			loop1:
			while (true) {
				int alt1=3;
				try { dbg.enterDecision(1, decisionCanBacktrack[1]);

				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==VAR) ) {
					alt1=1;
				}
				else if ( (LA1_0==BEGIN) ) {
					alt1=2;
				}

				} finally {dbg.exitDecision(1);}

				switch (alt1) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
					{
					dbg.location(17,8);
					pushFollow(FOLLOW_stat_in_prog49);
					stat1=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat1.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:15: blok
					{
					dbg.location(17,15);
					pushFollow(FOLLOW_blok_in_prog53);
					blok2=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok2.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt1++;
			}
			} finally {dbg.exitSubRule(1);}
			dbg.location(17,25);
			EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_prog57); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(17, 25);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "prog");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "prog"


	public static class blok_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "blok"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:1: blok : BEGIN ^ ( stat | blok )* END !;
	public final ExprParser.blok_return blok() throws RecognitionException {
		ExprParser.blok_return retval = new ExprParser.blok_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token BEGIN4=null;
		Token END7=null;
		ParserRuleReturnScope stat5 =null;
		ParserRuleReturnScope blok6 =null;

		CommonTree BEGIN4_tree=null;
		CommonTree END7_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "blok");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(19, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:6: ( BEGIN ^ ( stat | blok )* END !)
			dbg.enterAlt(1);

			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:8: BEGIN ^ ( stat | blok )* END !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(19,13);
			BEGIN4=(Token)match(input,BEGIN,FOLLOW_BEGIN_in_blok70); 
			BEGIN4_tree = (CommonTree)adaptor.create(BEGIN4);
			root_0 = (CommonTree)adaptor.becomeRoot(BEGIN4_tree, root_0);
			dbg.location(19,15);
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:15: ( stat | blok )*
			try { dbg.enterSubRule(2);

			loop2:
			while (true) {
				int alt2=3;
				try { dbg.enterDecision(2, decisionCanBacktrack[2]);

				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= ID && LA2_0 <= LP)||LA2_0==NL||LA2_0==VAR) ) {
					alt2=1;
				}
				else if ( (LA2_0==BEGIN) ) {
					alt2=2;
				}

				} finally {dbg.exitDecision(2);}

				switch (alt2) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:16: stat
					{
					dbg.location(19,16);
					pushFollow(FOLLOW_stat_in_blok74);
					stat5=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat5.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:23: blok
					{
					dbg.location(19,23);
					pushFollow(FOLLOW_blok_in_blok78);
					blok6=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok6.getTree());

					}
					break;

				default :
					break loop2;
				}
			}
			} finally {dbg.exitSubRule(2);}
			dbg.location(19,33);
			END7=(Token)match(input,END,FOLLOW_END_in_blok82); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(20, 0);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blok");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "blok"


	public static class stat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "stat"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:22:1: stat : ( expr NL -> expr | VAR ID ( PODST expr )? NL -> ^( VAR ID ) ( ^( PODST ID expr ) )? | ID PODST expr NL -> ^( PODST ID expr ) | if_stat NL -> if_stat | NL ->);
	public final ExprParser.stat_return stat() throws RecognitionException {
		ExprParser.stat_return retval = new ExprParser.stat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token NL9=null;
		Token VAR10=null;
		Token ID11=null;
		Token PODST12=null;
		Token NL14=null;
		Token ID15=null;
		Token PODST16=null;
		Token NL18=null;
		Token NL20=null;
		Token NL21=null;
		ParserRuleReturnScope expr8 =null;
		ParserRuleReturnScope expr13 =null;
		ParserRuleReturnScope expr17 =null;
		ParserRuleReturnScope if_stat19 =null;

		CommonTree NL9_tree=null;
		CommonTree VAR10_tree=null;
		CommonTree ID11_tree=null;
		CommonTree PODST12_tree=null;
		CommonTree NL14_tree=null;
		CommonTree ID15_tree=null;
		CommonTree PODST16_tree=null;
		CommonTree NL18_tree=null;
		CommonTree NL20_tree=null;
		CommonTree NL21_tree=null;
		RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
		RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
		RewriteRuleSubtreeStream stream_if_stat=new RewriteRuleSubtreeStream(adaptor,"rule if_stat");

		try { dbg.enterRule(getGrammarFileName(), "stat");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(22, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:5: ( expr NL -> expr | VAR ID ( PODST expr )? NL -> ^( VAR ID ) ( ^( PODST ID expr ) )? | ID PODST expr NL -> ^( PODST ID expr ) | if_stat NL -> if_stat | NL ->)
			int alt4=5;
			try { dbg.enterDecision(4, decisionCanBacktrack[4]);

			switch ( input.LA(1) ) {
			case INT:
			case LP:
				{
				alt4=1;
				}
				break;
			case ID:
				{
				int LA4_2 = input.LA(2);
				if ( (LA4_2==PODST) ) {
					alt4=3;
				}
				else if ( (LA4_2==DIV||(LA4_2 >= MINUS && LA4_2 <= PLUS)) ) {
					alt4=1;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 4, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VAR:
				{
				alt4=2;
				}
				break;
			case IF:
				{
				alt4=4;
				}
				break;
			case NL:
				{
				alt4=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(4);}

			switch (alt4) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:7: expr NL
					{
					dbg.location(23,7);
					pushFollow(FOLLOW_expr_in_stat96);
					expr8=expr();
					state._fsp--;

					stream_expr.add(expr8.getTree());dbg.location(23,12);
					NL9=(Token)match(input,NL,FOLLOW_NL_in_stat98);  
					stream_NL.add(NL9);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 23:15: -> expr
					{
						dbg.location(23,18);
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:7: VAR ID ( PODST expr )? NL
					{
					dbg.location(25,7);
					VAR10=(Token)match(input,VAR,FOLLOW_VAR_in_stat111);  
					stream_VAR.add(VAR10);
					dbg.location(25,11);
					ID11=(Token)match(input,ID,FOLLOW_ID_in_stat113);  
					stream_ID.add(ID11);
					dbg.location(25,14);
					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:14: ( PODST expr )?
					int alt3=2;
					try { dbg.enterSubRule(3);
					try { dbg.enterDecision(3, decisionCanBacktrack[3]);

					int LA3_0 = input.LA(1);
					if ( (LA3_0==PODST) ) {
						alt3=1;
					}
					} finally {dbg.exitDecision(3);}

					switch (alt3) {
						case 1 :
							dbg.enterAlt(1);

							// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:15: PODST expr
							{
							dbg.location(25,15);
							PODST12=(Token)match(input,PODST,FOLLOW_PODST_in_stat116);  
							stream_PODST.add(PODST12);
							dbg.location(25,21);
							pushFollow(FOLLOW_expr_in_stat118);
							expr13=expr();
							state._fsp--;

							stream_expr.add(expr13.getTree());
							}
							break;

					}
					} finally {dbg.exitSubRule(3);}
					dbg.location(25,28);
					NL14=(Token)match(input,NL,FOLLOW_NL_in_stat122);  
					stream_NL.add(NL14);

					// AST REWRITE
					// elements: ID, ID, PODST, expr, VAR
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 25:31: -> ^( VAR ID ) ( ^( PODST ID expr ) )?
					{
						dbg.location(25,34);
						// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:34: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(25,36);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(25,40);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}
						dbg.location(25,44);
						// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:44: ( ^( PODST ID expr ) )?
						if ( stream_ID.hasNext()||stream_PODST.hasNext()||stream_expr.hasNext() ) {
							dbg.location(25,44);
							// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:44: ^( PODST ID expr )
							{
							CommonTree root_1 = (CommonTree)adaptor.nil();
							dbg.location(25,46);
							root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
							dbg.location(25,52);
							adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(25,55);
							adaptor.addChild(root_1, stream_expr.nextTree());
							adaptor.addChild(root_0, root_1);
							}

						}
						stream_ID.reset();
						stream_PODST.reset();
						stream_expr.reset();

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:7: ID PODST expr NL
					{
					dbg.location(27,7);
					ID15=(Token)match(input,ID,FOLLOW_ID_in_stat148);  
					stream_ID.add(ID15);
					dbg.location(27,10);
					PODST16=(Token)match(input,PODST,FOLLOW_PODST_in_stat150);  
					stream_PODST.add(PODST16);
					dbg.location(27,16);
					pushFollow(FOLLOW_expr_in_stat152);
					expr17=expr();
					state._fsp--;

					stream_expr.add(expr17.getTree());dbg.location(27,21);
					NL18=(Token)match(input,NL,FOLLOW_NL_in_stat154);  
					stream_NL.add(NL18);

					// AST REWRITE
					// elements: PODST, ID, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 27:24: -> ^( PODST ID expr )
					{
						dbg.location(27,27);
						// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:27: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(27,29);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(27,35);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(27,38);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:28:7: if_stat NL
					{
					dbg.location(28,7);
					pushFollow(FOLLOW_if_stat_in_stat172);
					if_stat19=if_stat();
					state._fsp--;

					stream_if_stat.add(if_stat19.getTree());dbg.location(28,15);
					NL20=(Token)match(input,NL,FOLLOW_NL_in_stat174);  
					stream_NL.add(NL20);

					// AST REWRITE
					// elements: if_stat
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 28:18: -> if_stat
					{
						dbg.location(28,21);
						adaptor.addChild(root_0, stream_if_stat.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:30:7: NL
					{
					dbg.location(30,7);
					NL21=(Token)match(input,NL,FOLLOW_NL_in_stat191);  
					stream_NL.add(NL21);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 30:10: ->
					{
						dbg.location(31,5);
						root_0 = null;
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(31, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "stat");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "stat"


	public static class if_stat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "if_stat"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:1: if_stat : IF ^ expr THEN ! ( blok | expr ) ( ELSE ! ( blok | expr ) )? ;
	public final ExprParser.if_stat_return if_stat() throws RecognitionException {
		ExprParser.if_stat_return retval = new ExprParser.if_stat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token IF22=null;
		Token THEN24=null;
		Token ELSE27=null;
		ParserRuleReturnScope expr23 =null;
		ParserRuleReturnScope blok25 =null;
		ParserRuleReturnScope expr26 =null;
		ParserRuleReturnScope blok28 =null;
		ParserRuleReturnScope expr29 =null;

		CommonTree IF22_tree=null;
		CommonTree THEN24_tree=null;
		CommonTree ELSE27_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "if_stat");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(33, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:3: ( IF ^ expr THEN ! ( blok | expr ) ( ELSE ! ( blok | expr ) )? )
			dbg.enterAlt(1);

			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:5: IF ^ expr THEN ! ( blok | expr ) ( ELSE ! ( blok | expr ) )?
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(34,7);
			IF22=(Token)match(input,IF,FOLLOW_IF_in_if_stat213); 
			IF22_tree = (CommonTree)adaptor.create(IF22);
			root_0 = (CommonTree)adaptor.becomeRoot(IF22_tree, root_0);
			dbg.location(34,9);
			pushFollow(FOLLOW_expr_in_if_stat216);
			expr23=expr();
			state._fsp--;

			adaptor.addChild(root_0, expr23.getTree());
			dbg.location(34,18);
			THEN24=(Token)match(input,THEN,FOLLOW_THEN_in_if_stat218); dbg.location(34,20);
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:20: ( blok | expr )
			int alt5=2;
			try { dbg.enterSubRule(5);
			try { dbg.enterDecision(5, decisionCanBacktrack[5]);

			int LA5_0 = input.LA(1);
			if ( (LA5_0==BEGIN) ) {
				alt5=1;
			}
			else if ( (LA5_0==ID||(LA5_0 >= INT && LA5_0 <= LP)) ) {
				alt5=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(5);}

			switch (alt5) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:21: blok
					{
					dbg.location(34,21);
					pushFollow(FOLLOW_blok_in_if_stat222);
					blok25=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok25.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:26: expr
					{
					dbg.location(34,26);
					pushFollow(FOLLOW_expr_in_if_stat224);
					expr26=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr26.getTree());

					}
					break;

			}
			} finally {dbg.exitSubRule(5);}
			dbg.location(34,32);
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:32: ( ELSE ! ( blok | expr ) )?
			int alt7=2;
			try { dbg.enterSubRule(7);
			try { dbg.enterDecision(7, decisionCanBacktrack[7]);

			int LA7_0 = input.LA(1);
			if ( (LA7_0==ELSE) ) {
				alt7=1;
			}
			} finally {dbg.exitDecision(7);}

			switch (alt7) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:33: ELSE ! ( blok | expr )
					{
					dbg.location(34,37);
					ELSE27=(Token)match(input,ELSE,FOLLOW_ELSE_in_if_stat228); dbg.location(34,39);
					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:39: ( blok | expr )
					int alt6=2;
					try { dbg.enterSubRule(6);
					try { dbg.enterDecision(6, decisionCanBacktrack[6]);

					int LA6_0 = input.LA(1);
					if ( (LA6_0==BEGIN) ) {
						alt6=1;
					}
					else if ( (LA6_0==ID||(LA6_0 >= INT && LA6_0 <= LP)) ) {
						alt6=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 6, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}

					} finally {dbg.exitDecision(6);}

					switch (alt6) {
						case 1 :
							dbg.enterAlt(1);

							// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:40: blok
							{
							dbg.location(34,40);
							pushFollow(FOLLOW_blok_in_if_stat232);
							blok28=blok();
							state._fsp--;

							adaptor.addChild(root_0, blok28.getTree());

							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:45: expr
							{
							dbg.location(34,45);
							pushFollow(FOLLOW_expr_in_if_stat234);
							expr29=expr();
							state._fsp--;

							adaptor.addChild(root_0, expr29.getTree());

							}
							break;

					}
					} finally {dbg.exitSubRule(6);}

					}
					break;

			}
			} finally {dbg.exitSubRule(7);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(35, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "if_stat");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "if_stat"


	public static class expr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:37:1: expr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
	public final ExprParser.expr_return expr() throws RecognitionException {
		ExprParser.expr_return retval = new ExprParser.expr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUS31=null;
		Token MINUS33=null;
		ParserRuleReturnScope multExpr30 =null;
		ParserRuleReturnScope multExpr32 =null;
		ParserRuleReturnScope multExpr34 =null;

		CommonTree PLUS31_tree=null;
		CommonTree MINUS33_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "expr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(37, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
			dbg.enterAlt(1);

			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(38,7);
			pushFollow(FOLLOW_multExpr_in_expr252);
			multExpr30=multExpr();
			state._fsp--;

			adaptor.addChild(root_0, multExpr30.getTree());
			dbg.location(39,7);
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
			try { dbg.enterSubRule(8);

			loop8:
			while (true) {
				int alt8=3;
				try { dbg.enterDecision(8, decisionCanBacktrack[8]);

				int LA8_0 = input.LA(1);
				if ( (LA8_0==PLUS) ) {
					alt8=1;
				}
				else if ( (LA8_0==MINUS) ) {
					alt8=2;
				}

				} finally {dbg.exitDecision(8);}

				switch (alt8) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:9: PLUS ^ multExpr
					{
					dbg.location(39,13);
					PLUS31=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr262); 
					PLUS31_tree = (CommonTree)adaptor.create(PLUS31);
					root_0 = (CommonTree)adaptor.becomeRoot(PLUS31_tree, root_0);
					dbg.location(39,15);
					pushFollow(FOLLOW_multExpr_in_expr265);
					multExpr32=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr32.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:9: MINUS ^ multExpr
					{
					dbg.location(40,14);
					MINUS33=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr275); 
					MINUS33_tree = (CommonTree)adaptor.create(MINUS33);
					root_0 = (CommonTree)adaptor.becomeRoot(MINUS33_tree, root_0);
					dbg.location(40,16);
					pushFollow(FOLLOW_multExpr_in_expr278);
					multExpr34=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr34.getTree());

					}
					break;

				default :
					break loop8;
				}
			}
			} finally {dbg.exitSubRule(8);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(42, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "expr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "expr"


	public static class multExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multExpr"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:44:1: multExpr : atom ( MUL ^ atom | DIV ^ atom )* ;
	public final ExprParser.multExpr_return multExpr() throws RecognitionException {
		ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MUL36=null;
		Token DIV38=null;
		ParserRuleReturnScope atom35 =null;
		ParserRuleReturnScope atom37 =null;
		ParserRuleReturnScope atom39 =null;

		CommonTree MUL36_tree=null;
		CommonTree DIV38_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "multExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(44, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:5: ( atom ( MUL ^ atom | DIV ^ atom )* )
			dbg.enterAlt(1);

			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:7: atom ( MUL ^ atom | DIV ^ atom )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(45,7);
			pushFollow(FOLLOW_atom_in_multExpr304);
			atom35=atom();
			state._fsp--;

			adaptor.addChild(root_0, atom35.getTree());
			dbg.location(46,7);
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:7: ( MUL ^ atom | DIV ^ atom )*
			try { dbg.enterSubRule(9);

			loop9:
			while (true) {
				int alt9=3;
				try { dbg.enterDecision(9, decisionCanBacktrack[9]);

				int LA9_0 = input.LA(1);
				if ( (LA9_0==MUL) ) {
					alt9=1;
				}
				else if ( (LA9_0==DIV) ) {
					alt9=2;
				}

				} finally {dbg.exitDecision(9);}

				switch (alt9) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:9: MUL ^ atom
					{
					dbg.location(46,12);
					MUL36=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr314); 
					MUL36_tree = (CommonTree)adaptor.create(MUL36);
					root_0 = (CommonTree)adaptor.becomeRoot(MUL36_tree, root_0);
					dbg.location(46,14);
					pushFollow(FOLLOW_atom_in_multExpr317);
					atom37=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom37.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:47:9: DIV ^ atom
					{
					dbg.location(47,12);
					DIV38=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr327); 
					DIV38_tree = (CommonTree)adaptor.create(DIV38);
					root_0 = (CommonTree)adaptor.becomeRoot(DIV38_tree, root_0);
					dbg.location(47,14);
					pushFollow(FOLLOW_atom_in_multExpr330);
					atom39=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom39.getTree());

					}
					break;

				default :
					break loop9;
				}
			}
			} finally {dbg.exitSubRule(9);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(49, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "multExpr"


	public static class atom_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:51:1: atom : ( INT | ID | LP ! expr RP !);
	public final ExprParser.atom_return atom() throws RecognitionException {
		ExprParser.atom_return retval = new ExprParser.atom_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token INT40=null;
		Token ID41=null;
		Token LP42=null;
		Token RP44=null;
		ParserRuleReturnScope expr43 =null;

		CommonTree INT40_tree=null;
		CommonTree ID41_tree=null;
		CommonTree LP42_tree=null;
		CommonTree RP44_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "atom");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(51, 0);

		try {
			// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:52:5: ( INT | ID | LP ! expr RP !)
			int alt10=3;
			try { dbg.enterDecision(10, decisionCanBacktrack[10]);

			switch ( input.LA(1) ) {
			case INT:
				{
				alt10=1;
				}
				break;
			case ID:
				{
				alt10=2;
				}
				break;
			case LP:
				{
				alt10=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(10);}

			switch (alt10) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:52:7: INT
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(52,7);
					INT40=(Token)match(input,INT,FOLLOW_INT_in_atom356); 
					INT40_tree = (CommonTree)adaptor.create(INT40);
					adaptor.addChild(root_0, INT40_tree);

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:53:7: ID
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(53,7);
					ID41=(Token)match(input,ID,FOLLOW_ID_in_atom364); 
					ID41_tree = (CommonTree)adaptor.create(ID41);
					adaptor.addChild(root_0, ID41_tree);

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/Pulpit/ANTLR/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:54:7: LP ! expr RP !
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(54,9);
					LP42=(Token)match(input,LP,FOLLOW_LP_in_atom372); dbg.location(54,11);
					pushFollow(FOLLOW_expr_in_atom375);
					expr43=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr43.getTree());
					dbg.location(54,18);
					RP44=(Token)match(input,RP,FOLLOW_RP_in_atom377); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(55, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "atom");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "atom"

	// Delegated rules



	public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x0000000000084F10L});
	public static final BitSet FOLLOW_blok_in_prog53 = new BitSet(new long[]{0x0000000000084F10L});
	public static final BitSet FOLLOW_EOF_in_prog57 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BEGIN_in_blok70 = new BitSet(new long[]{0x0000000000084F90L});
	public static final BitSet FOLLOW_stat_in_blok74 = new BitSet(new long[]{0x0000000000084F90L});
	public static final BitSet FOLLOW_blok_in_blok78 = new BitSet(new long[]{0x0000000000084F90L});
	public static final BitSet FOLLOW_END_in_blok82 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_stat96 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat98 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat111 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_ID_in_stat113 = new BitSet(new long[]{0x0000000000014000L});
	public static final BitSet FOLLOW_PODST_in_stat116 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_expr_in_stat118 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat122 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_stat148 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_PODST_in_stat150 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_expr_in_stat152 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat154 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_if_stat_in_stat172 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat174 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NL_in_stat191 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_if_stat213 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_expr_in_if_stat216 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_THEN_in_if_stat218 = new BitSet(new long[]{0x0000000000000D10L});
	public static final BitSet FOLLOW_blok_in_if_stat222 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_expr_in_if_stat224 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_ELSE_in_if_stat228 = new BitSet(new long[]{0x0000000000000D10L});
	public static final BitSet FOLLOW_blok_in_if_stat232 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_if_stat234 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multExpr_in_expr252 = new BitSet(new long[]{0x0000000000009002L});
	public static final BitSet FOLLOW_PLUS_in_expr262 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_multExpr_in_expr265 = new BitSet(new long[]{0x0000000000009002L});
	public static final BitSet FOLLOW_MINUS_in_expr275 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_multExpr_in_expr278 = new BitSet(new long[]{0x0000000000009002L});
	public static final BitSet FOLLOW_atom_in_multExpr304 = new BitSet(new long[]{0x0000000000002022L});
	public static final BitSet FOLLOW_MUL_in_multExpr314 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_atom_in_multExpr317 = new BitSet(new long[]{0x0000000000002022L});
	public static final BitSet FOLLOW_DIV_in_multExpr327 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_atom_in_multExpr330 = new BitSet(new long[]{0x0000000000002022L});
	public static final BitSet FOLLOW_INT_in_atom356 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_atom364 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LP_in_atom372 = new BitSet(new long[]{0x0000000000000D00L});
	public static final BitSet FOLLOW_expr_in_atom375 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_RP_in_atom377 = new BitSet(new long[]{0x0000000000000002L});
}
