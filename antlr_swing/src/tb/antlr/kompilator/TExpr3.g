tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer wynik = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

zakres : ^(BEGIN {enterScope();} (e+=zakres | e+=expr | d+=decl)* {leaveScope();} ) -> blok(wyr={$e},dekl={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
 /*   
set :
  ^(PODST ID INT) {globals.setSymbol( $ID.text, Integer.parseInt($INT.text) );} -> newglob(n={$ID.text}, v={$INT.text})
  ;
*/
expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) 
        | ^(MUL   e1=expr e2=expr) 
        | ^(DIV   e1=expr e2=expr) 
        | ^(PODST i1=ID   INT) {globals.setSymbol( $ID.text, Integer.parseInt($INT.text));} -> newglob(n={$ID.text}, v={$INT.text})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
        | ID -> id(n={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> if(e1={$e1.st},e2={$e2.st},e3={$e3.st}, nr={numer.toString()})
        | ADD {wynik=globals.addVars();} -> add(w={wynik.toString()})
        | SUB {wynik=globals.subVars();} -> sub(w={wynik.toString()})
        | DIVI {wynik=globals.divVars();} -> divi(w={wynik.toString()})
        | MULT {wynik=globals.mulVars();} -> mult(w={wynik.toString()})
        | ^(MOVA i1=ID) {wynik=globals.movA($ID.text);} -> mova(w={wynik.toString()})
        | ^(MOVN i1=ID) {wynik=globals.movN($ID.text);} -> movn(w={wynik.toString()})
        | ^(HASH i1=ID) {wynik=globals.getSymbol($ID.text);} -> showvar(w={wynik.toString()},n={$ID.text})
    ;
    